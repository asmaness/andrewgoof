from sys import exit

# here is the starting point -- we know it is such because of the final line of the
# program. setting the scene and the user picks a room. you are thrown into that
# room. if you don't pick a room, you die...
def start():
    print "You are in a dark room."
    print "There is a door to your right and left."
    print "Which one do you take?"

    choice = raw_input("> ")

    if choice == "left":
        bear_room()
    elif choice == "right":
        cthulhu_room()
    else:
        dead("You stumble around the room until you starve. Bye.")

# the above rooms have options in which the user dies. this explains what happens
# specifically when those choices are made. all it does is print out the reason as
# stated in the room function, add a snotty remark, and exit the program.
def dead(why):
    print why , "Good job!"
    exit(0)

# each fcn represents a room. gold room here. you take some gold. if too little or
# too much, you die. gotta find the sweet spot, yo... if you do, you win!!
def gold_room():
    print "This room is full of gold bars. How many do you take?"

    how_much = eval(raw_input("> "))

    if how_much <= 1:
        dead("You're too nice. You die from altruism.")
    if how_much > 1 and how_much < 50:
        print "Nice. You're not greedy. You win..."
        exit(0)
    else:
        dead("You greedy bastard! You die from selfishness.")

# the bear room (the below defined fcn) has a bear guarding a door. two things here
# that influence the outcome. whether the bear moved + the user's choice... if you
# take honey, you die. if you taunt the bear, you'll be fine the first time. but 
# the second time, you're in trouble because that variable is now true. you'll die.
# simply opening the door and ignoring the bear will place you into the gold room.
def bear_room():
    print "There is a bear here."
    print "The bear has a bunch of honey."
    print "The fat bear is in front of another door."
    print "How are you going to move the bear?"
    bear_moved = False

    while True:
        choice = raw_input("> ")

        if choice == "take honey":
            dead("The bear looks at you then slaps your face off.")
        elif choice == "taunt bear" and not bear_moved:
            print "The bear has moved from the door. You can go through it now."
            bear_moved = True
        elif choice == "taunt bear" and bear_moved:
            dead("The bear gets pissed off and chews off your leg.")
        elif choice == "open door" and bear_moved:
            gold_room()
        else:
            print "I got no idea what that means."

# this friggin room is pretty straightforward. you either eat your head and die
# or leave back to the start of the "maze"...
def cthulhu_room():
    print "Here you see the great evil Cthulhu."
    print "He, it, whatever stares at you and you go insane."
    print "Do you flee for your life or eat your head?"

    choice = raw_input("> ")

    if "flee" in choice:
        start()
    elif "head" in choice:
        dead("Well, that was tasty. Hue, hue, hue.")
    else:
        cthulhu_room()

# this is the starting fcn of our program...
start()