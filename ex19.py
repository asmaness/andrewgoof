# so here, we are defining a function called "cheese_and
# _crackers"... there are two inputs. after you execute,
# we print the number of cheeses, number of crackers, and
# some fucking dumb humor...
def cheese_and_crackers(cheese_count , boxes_of_crackers):
    print "You have %d cheeses." % cheese_count
    print "You have %d boxes of crackers." % boxes_of_crackers
    print "Man... that's enough for a party."
    print "Get a blanket. #HueHueHue\n"

# an example demonstrating directly-inserted inputs...
print "We can just give the function numbers directly:"
cheese_and_crackers(20 , 30)

# now we can assign variables and call on them for inputs...
print "OR, we can use variables from our script:"
amount_of_cheese = 10
amount_of_crackers = 50
cheese_and_crackers(amount_of_cheese , amount_of_crackers)

# showing that math can be calculated as inputs:
print "We can even do math inside, too:"
cheese_and_crackers(10 + 20 , 5+6)

# finally, combining the previous to forms of inputs...
print "And we can combine the two..."
cheese_and_crackers(amount_of_cheese+100 , 
    amount_of_crackers+17)
