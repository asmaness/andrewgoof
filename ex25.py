# some over-arching notes: the triple quotations provide documentation comments
# when we inquire (type in "help") for a particular fcn or the entire ex25...

#from ex25 import *

# break_words fcn will essentially take each word (and the puncuation attached)
# and split the sentence / store each word/token in a single variable called words.
def break_words(stuff):
    """This function will break up words for us."""
    words = stuff.split(' ')
    return words

# sort_words (so... ex25.sort_words() ) puts words in alphabetical order
def sort_words(words):
    """Sorts the words."""
    return sorted(words)

# print_first_word does a couple of things -- it takes the first word from the
# user input variable and then peels it off from the rest of the variable...
# the result is a group of words (the sentence) void of the first word.
def print_first_word(words):
    """Prints the first word after popping it off."""
    word = words.pop(0)
    print word

# similar to previous fcn, but the big takeway here is that "-1" allows you to
# count characters from the end (-1 is last, -2 second-to-last, etc.)
def print_last_word(words):
    """Prints the last word after popping it off"""
    word = words.pop(-1)
    print word

# sort_sentence will sort all of the tokens/words
def sort_sentence(sentence):
    """Takes in a full sentence and returns the sorted words."""
    words = break_words(sentence)
    return sort_words(words)

def print_first_and_last(sentence):
    """Prints the first and last words of the sentence."""
    words = break_words(sentence)
    print_first_word(words)
    print_last_word(words)

def print_first_and_last_sorted(sentence):
    """Sorts the words then prints the first and last one."""
    words = sort_sentence(sentence)
    print_first_word(words)
    print_last_word(words)