# adding two inputs, a and b. printing to noify user that we are adding and
# then using return fcn to actually compute a+b
def add(a , b):
    print "ADDING %d + %d" % (a , b)
    return a + b

# similar to above, subtracting. I use %f because i want to use a decimal
# instead of integer in this concrete example. float formatting allows the
# user to see the non-integer values...
def subtract(a , b):
    print "SUBTRACTING %f - %f" % (a , b)
    return a - b

# multiply -- similar to above...
def multiply(a , b):
    print "MULTIPLYING %d * %d" % (a , b)
    return a * b

# dividing -- operations like the first three...
def divide(a , b):
    print "DIVIDING %d / %d" % (a , b)
    return a / b

# diving into the actual output; silly title (read: lame)
print "Let's do some math with just functions, eh!?"

# defining four descrpitive variables by using our created fcns...
age = add(20 , 6)
height = subtract(77 , 4.5)
weight = multiply(31 , 5)
iq = divide(100 , 2)

# printing the results -- notice how number formats are different, but
# we can still group them in the same parenthetical...
print "Age: %d, Height: %f, Weight: %d, IQ: %d" % (age, height, weight, iq)

# credito addicional...
print "Here is a puzzle..."

what = add(age , subtract(height , multiply(weight , divide(iq , 2))))

print "That becomes: " , what , "... Kenya do it by hand?"