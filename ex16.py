from sys import argv

script , filename = argv

print "We're going to erase %r." % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you want that, hit RETURN."

raw_input("? ")

# opening the file; the "w" in the open function signals that
# we are preparing to write. "r" would be read, for example.
print "Opening the file..."
target = open(filename , 'w')

# truncating the file, but not necessary for writing an opened
# file... this would be necessary if no "w" option above; still,
# good practice, though...
#print "Truncating the file. Goodbye!"
#target.truncate()

print "Now I'm going to ask you for three lines."

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")
print "I'm going to write these to the file."

target.write("%s\n%s\n%s\n" % (line1 , line2 ,line3))

print "And finally, we close it."
target.close()

# review from last exercise... opening and priniting file
print "\nLet's open, print, and close the file..."
x = open(filename)
print x.read()
x.close()