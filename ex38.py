ten_things = "Apples Oranges Crows Telephone Light Sugar"

print "Wait. There are not 10 things in that list. Let's fix that."

stuff = ten_things.split(" ")
more_stuff = ["Day" , "Night" , "Song" , "Frisbee" , "Corn" , "Banana" , "Girl" , "Boy"]


while len(stuff) !=  10: # so long as there are not 10 items...
    next_one = more_stuff.pop() # taking 'more_stuff' and popping off the final token -- call it 'next_one'
    # the equivalent here is 'pop(more_stuff)'
    print "Adding: " , next_one
    stuff.append(next_one) # taking 'stuff' (our ten things split into tokens) and appending 'next_one'
    print "There are %d items now." % len(stuff) # denoting the length of 'stuff'

print "There we go... " , stuff

print "Let's do some things with 'stuff'..."

print stuff[1]
print stuff[-1]
print stuff.pop()
print ' '.join(stuff)
print '#'.join(stuff[3:5])