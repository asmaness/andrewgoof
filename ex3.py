# We're counting our chickens -- this titles it...
print "I will not count my chickens:"

# number of female chickens...
print "Hens" , 25 + 30 / 6

# number of male chickens...
print "Roosters" , 100 - 25 * 3 % 4

# titling the egg-counting section
print "Now I will count the eggs:"

# computing the number of eggs
print float(3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6)

# asking whether an inequality is true
print "Is it true that 3 + 2 < 5 - 7?"

# printing the actual answer to that ineqaulity (false or true)
print 3 + 2 < 5 - 7

# asking what the lhs equals
print "What is 3 + 2?" , 3 + 2

# asking what the rhs equals
print "What is 5 - 7" , 5 - 7

# side commentary that confirms false hypothesis
print "Oh , that's why it's False."

# we're going to do one more practice...
print "How about some more?"

# true or false: is 5 greater than -2
print "Is it greater?" , 5 > -2

# t or f: is 5 >= -2?
print "Is it greater or equal?" , 5 >= -2

# t or f: is 5 less or eqaul to -2?
print "Is it less or equal?" , 5 <= -2