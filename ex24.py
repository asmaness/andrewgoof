# a simple title and then a line that shows-off our string printing skills; the big
# lesson here is knowing that the backward-slash is your friend in that it can intro-
# duce new lines (\n), indent / tab (\t), and print literal characters like " ' " and
# " \ ". All in just one line!!!
print "Let's practice everything we've studied so far..."
print 'You\'d need to know \'bout escapes with \\ that do \n new lines and \t tabs.'

# more practice / review -- the triple quotes allow us to (1) write string perpetually,
# and (2) print it out exactly how it is in the code, i.e. when we break a line, a line
# is broken in the output. also reviewing more fancy backslash techniques...
poem = """
\tThe lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an explanation
\n\t\twhere there is none.
"""

# printing out our (K.C.) masterpiece
print "------------------"
print poem
print "------------------"

# basic math practice and then confirming the answer via the print command
five = 10 - 2 + 3 - 6
print "This should be five: %s." % five

# defining our own fcn called "secret_formula", which calculates the number
# of 'beans as 500 times the starting point, assumes a jar contains 1000 jelly
# beans, and that a crate holds 100 jars (or 100,000 jelly beans)... this fcn
# will return the number of beans, jars, and crates...
def secret_formula(started):
    jelly_beans = started * 500
    jars = jelly_beans / 1000
    crates = jars / 100
    return jelly_beans , jars , crates

# listing the starting point and then calculating the value of the three
# desired output variables
start_point = 10000
beans , jars , crates = secret_formula(start_point)

# now we show our output for the user -- listing the starting point, which
# is determined within our py-file and is 10,000. and then we list the
# output variables -- 'beans, jars, and crates...
print "With a starting point of: %d" % start_point
print "We'd have %d beans, %d jars, and %d crates." % (beans, jars, crates)

# now we decrease the starting point tenfold.
start_point = start_point / 10
print "We can also do that this way:"
print "We'd have %d beans, %d jars, and %d crates." % secret_formula(start_point)