# an "import" ... this is how you add features from the
# Python feature set. keeps your programs small, but also 
# acts as documentaiton for others who are reading.
#
# argv is the "argument variable" -- it holds arguments 
# you pass thry the python script (py-file...) 

# adding a user-input feature as review...
argv = raw_input("What is the name of your module? ")
from sys import argv

# unpacking the "argv" argument so that argv gets assigned
# to the 4 variables that we define below. in other words,
#
# "take whatever is on argv, unpack it, and assign it to
# all of these variables on the left in order."
script , first , second , third = argv

# NOTE: features are really called "modules"... so you
# import <module>. also known as libraries.
print "The script is called:" , script
print "Your first variable is:" , first
print "Your second variable is:" , second
print "Your third variable is:" , third

# when executing this py-file, it is important to name
# three variables - no more, no less - or else there will
# be an error (i.e., must have 1:1 mapping...)

# *** MODULES GIVE YOU FEATURES! ***