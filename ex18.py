# *** WE'RE GOING TO DEFINE SOME FUNCTIONS, WOOOOO! ***

# this one is like previous scripts with argv
def print_two(*args):
    arg1 , arg2 = args
    print "arg1: %r , arg2: %r" % (arg1 , arg2)

# that was pointless, but we can just do this...
def print_two_again(arg1 , arg2):
    print "arg1: %r , arg2: %r" % (arg1 , arg2)

# this just takes one argument
def print_one(arg1):
    print "arg1: %r" % arg1

# and this one takes no arguments! HA...
def print_none():
    print "I got nothin', yo!"


print_two("Andrew" , "Maness")
print_two_again("Andrew" , "Maness")
print_one("First!")
print_none()