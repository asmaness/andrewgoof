# we're practicing single/double quotes in our programs here...

# we learn here that "\t" indents the output string
tabby_cat = "\tI'm tabbed in."

# reviewing that "\n" will split the string into 2 lines
persian_cat = "I'm split\non a line."

# a double-backslash allows us to actually output a single backslash in output
backslash_cat = "I'm \\ a \\ cat."

# combining our triple-quote trick with indentations to make a list...
# looks like triple single quotes produces same outcome...
fat_cat = '''
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
'''

# just printing out all of our mini-exercises:
print tabby_cat
print persian_cat
print backslash_cat
print fat_cat
