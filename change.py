# change.py
#   
#   This program calculates the total amount of money one
#   has given the number and types of coins the user inputs.
#   The big lesson, however, is understanding and learning
#   how to appropriately format values, i.e. dollar amounts.
#
#   Created by Andrew S. Maness - 01/03/15.
#

def main():

  print "This is a change counter...\n"

  print "Please enter the count of each type of coin."
  quarters = eval(raw_input("   Quarters: "))
  dimes    = eval(raw_input("   Dimes:    "))
  nickels  = eval(raw_input("   Nickels:  "))
  pennies  = eval(raw_input("   Pennies:  "))

  total = quarters*.25 + dimes*.10 + nickels*.05 + pennies*.01
  x = 34
  print "The total value of your change is ${:.2f} and Joel has {} apples.".format(total,x)

main()
