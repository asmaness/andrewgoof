# this exercise is going to add and argv and "raw_input"
# together...

# importing a module called argv 
from sys import argv

# declaring a username from the module; now you must command
# with a username (or any name, really...)
script , user_name, comp_name = argv

# this is going to print where we tell it to; if surrounded
# by raw_input, it'll allow the user to respond approrpiately.
prompt = 'Your response is -- '

# the first line of print 
print "Hi %s, I'm the %s script." % (user_name , script)
print "You may call me %s." % (comp_name)
print "I'd like to ask you a few questions."
print "Do you like %s, %s? " % (comp_name , user_name)
likes = raw_input(prompt)

print "Where do you live, %s? " % user_name
lives = raw_input(prompt)

print "What kind of computer do you have? "
computer = raw_input(prompt)

print """
Alright, so you said %r about liking me.
You live in %r. Not sure where *that* is...
And you have a %r computer. #NICE
- %r.
""" % (likes , lives , computer, comp_name)