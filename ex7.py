# the first three lines print out a poem -- each line in the code
# corresponds to a line that will be printed out (i.e., the "print"
# command); no commas after the commands, so will print out one line
# at a time.
print "Mary had little lamb."
print "Its fleece was white as %s." % 'snow'
print "And everywhere that Mary went."

# this line adds ten periods immediately after the poem
print "." * 10 # adds 10 periods directly underneath the previous 3 lines...

# the following 12 lines assign a letter to each generated variables. note
# that in theory we could use "end3" for all e's, etc...
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

#spell out the previous several letters onto two lines:
# note the comma at the end of the next line -- it assures that the following
# printed code-line will be output onto the same line, only separated by a space.
print end1 + end2 + end3 + end4 +end5 + end6,
print end7 + end8 +end9 + end10 + end11 +end12