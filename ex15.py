# this is the python file for exercise 15; there is a sample
# text follow that accompanies it. we will open that thru
# this script...

# importing the argv module (again) and assigning a variable
# called filename that will be used to open our sample text
from sys import argv
script , filename = argv

# the open command will open the filename input in the 
# terminal prompt...
txt = open(filename)

# listing the name of the file and then reading it. NOTE:
# the period denotes that the command after the dot is for
# the file listed before the dot. here, we want to read
# the sample text...
print "Here's your file %r: " % filename
print txt.read()

# review over raw input exercises. then we can input the name
# of the file again and open it again. then we'll read it one
# more time. repitition, y'all...
print "Type the filename again: "
file_again = raw_input("> ")
txt_again = open(file_again)
print txt_again.read()

print txt.close()
print txt_again.close()