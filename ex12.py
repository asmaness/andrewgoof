# this is similar to the previous exercise, but provides a more efficient
# manner of gathering user input through the raw_input command...

# adding the user input prompts...
age = raw_input("How old are you? ")
height = raw_input("How tall are you? ")
weight = raw_input("How much do you weigh? ")

# printing out the info... just like last exercise
print "So, you're %s old, %s tall, and %s heavy. Ya, dork..." % (
    age, height, weight)