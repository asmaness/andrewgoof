# quadratic.py
#
# in this program, we'll solve a 2-degree function with
# the help of python's "math" library.
#
# created by andrew s. maness
#

# making the math library available
import math 

def main():

  print "This program finds the non-imaginary solutions to a quadratic equation."
  print " "

  a = eval(raw_input("What is the coefficient for your second-degree term? ")) 
  b = eval(raw_input("What is the coefficient for your first-degree term?  "))
  c = eval(raw_input("What is the value of your zero-degree term?          "))

  roots = math.sqrt(b*b - 4*a*c)
  root1 = (-b + roots) / (2*a)
  root2 = (-b - roots) / (2*a)

  print " "
  print "Your real answers are" , root1 , "and" , root2 , "."

main()

