# going to start working on user input programs...


print "How old are you? " , 
age = raw_input()
print "How tall are you? " , 
height = raw_input()
print "How much do you weigh? " , 
weight = raw_input()

# ol' zed specified %r, but I like %s better here, especially
# since we're producing a "nice" output and not trying to find 
# a particular coding error or whatever...
print "So, you're %s old, %s tall, and %s heavy." % (
    age, height, weight)