from sys import argv

script , input_file = argv

# this function that we are defining is called "print_all";
# the goal here is to read the contents of the input file.
def print_all(f):
    print f.read()

# the rewind function goes to the 0th byte of the file, i.e.
# the beginning of our input file...
def rewind(f):
    f.seek(0)

# this function prints the desired line of our input file.
def print_a_line(line_count , f):
    print line_count , f.readline()

# so we open the input file, let the reader know that we are
# opening, and then run the print_all and rewind functions.
current_file = open(input_file)
print "First, let's print the whole file:\n"
print_all(current_file)
print "Now, let's rewind, kind of like a tape.\n"
rewind(current_file)

# now we want to print each line individually from our print-
# a-line command. the "+ 1" when replacing the current_line 
# variable simply increases the line # by 1...
print "Let's print three lines:\n"

current_line = 1
print_a_line(current_line , current_file)

current_line = current_line + 1
print_a_line(current_line , current_file)

current_line = current_line + 1
print_a_line(current_line , current_file)