from sys import exit
from random import randint

#* Map
#    * green flag
#    * next scene
#* Engine
#    * play
#* Event
#    - enter
#    * Wreck
#    * Poor-handling car
#    * Slow pitstop
#    * Dialed-in setup
#    * Fast pit crew
#    * Victory lane
#    * Garage

class Event(object):

    def enter(self):
        print "This event is not yet configured. Subclass it and implement enter()."
        exit(1)

class Engine(object):

    def __init__(self, event_map):
        self.event_map = event_map

    def play(self):
        current_event = self.event_map.opening_event()
        last_event = self.event_map.next_event('win')

        while current_event != last_event:
            next_event_name = current_event.enter()
            current_event = self.event_map.next_event(next_event_name)

        # be syre to print out the last scene
        current_event.enter()

class Lose(Event):

    def enter(self):
        print "Race over. Maybe next week."
        exit(1)

class Neutral(Event):

    def enter(self):
        print """
        You're in a NASCAR event. And just like the real thing, a lot of this is up to chance, i.e.
        my Python code. But you play anyway because you like stock-car racing. Alright. You're
        commencing today's endeavor from a mid-pack starting spot. Do you opt to run this initial
        stint 'aggressively' or 'patiently'?
        """

        action = raw_input(">>> ")
        chance = randint(1,4)

        if action == "aggressively" and (chance == 1 or chance == 3) :
            print "A risky move... let's see what happens...\n"
            return "wreck"

        if action == "aggressively" and (chance == 2 or chance == 4) :
            print "Your aggressiveness works and you set a torrid pace toward the front.\n"
            return "goodcar"

        if action == "patiently" and (chance == 1 or chance == 4) :
            print "A bland strategy, but it'll do.\n"
            return "badcar"

        if action == "patiently" and (chance == 2 or chance == 3) :
            print "A bland strategy, but it'll do.\n"
            return "goodcar"

        else:
            print "Choose 'aggressively' or 'patiently', dude..."
            return "neutral"

class Win(Event):

    def enter(self):
        print "Congratulations on your victory. But just like reality, it may not mean a damn"
        print "thing next week or for the rest of the season. Good luck, though."
        exit(1)

class Wreck(Event):

    def enter(self):
        print "Nothing seems to be working well at this point. Do you 'tough' it out or"
        print "come down 'pit' road for fuel, new tires, and setup adjustments?"
        
        action = raw_input(">>> ")
        chance = randint(1,2)

        if action == "tough" and chance==1:
            print "You run out of talent, slap the wall, and lose your girlfriend.\n"
            return "lose"

        if action == "tough" and chance==2:
            print "You manage to keep the car off the wall, but car is junk.\n"
            return "badcar"

        if action == "pit" and chance==1:
            print "A noble decision.\n"
            return "fastcrew"

        if action == "pit" and chance==2:    
            print "A noble decision.\n"
            return "slowstop"

        else:
            print "Choose 'tough' or 'pit', bro...\n"
            return "wreck" 

class Badcar(Event):

    def enter(self):
        print "Your car is driving like junk and you're losing positions by the lap. Do you"
        print "'pit' in hopes that you fix the ailment or 'stay' out and trust that the car"
        print "will improve over the course of the current stint?"
        
        action = raw_input(">>> ")
        chance = randint(1,2)

        if action == "pit" and chance==1:
            print "You lose some track position, but your car gets dialed-in and you advance"
            print "toward the front of the field over time. Good call.\n"
            return "goodcar"

        if action == "pit" and chance==2:
            print "Your crewmen are terribly slow and cost you several seconds on the track.\n"
            return "slowstop"

        if action == "stay" and chance==1:
            print "This is fantastic. Your car comes into its own in the long run and you"
            print "begin motoring your way to the front, just like Josh Browne.\n"
            return "goodcar"

        if action == "stay" and chance==2:
            print "Tough sledding. The car actually gets worse. Keep your chin up, kid.\n"
            return "wreck"

        else:
            print "Choose 'pit' or 'stay', darnit!\n"
            return "badcar"

class Slowstop(Event):

    def enter(self):
        print "The slow stop has you mired in traffic with an ill-handling car. Do you"
        print "race 'aggressively' to compensate or stay 'patient'?"

        action = raw_input(">>> ")
        chance = randint(1,2)

        if action == "aggressively":
            print "You're getting flustered. You keep making mistakes to the point that you're"
            print "in some major trouble. Tread carefully.\n"
            return "wreck"

        if action == "patient":
            print "Your mental strategy seems to work. The field thins-out and your car improves.\n"
            return "goodcar"

        else:
            print "You have two choices here -- 'aggressively' or 'patient'. Pick one.\n"
            return "slowstop"

class Goodcar(Event):

    def enter(self):
        print "Your car is running well, you feel great, and you just might have a shot to win."
        print "Do you 'keep' the plan as-is, or 'tinker' with the car on your next stop?"

        action = raw_input(">>> ")
        chance = randint(1,6)

        if action == "keep" and chance==6:
            print "Surprisingly, the rest of the race is pretty uneventful and you win!\n"
            return "win"
        
        if action == "keep" and chance<3:
            print "The car starts to slip a bit and you realize changes may need to occur.\n"
            return "badcar"
        
        if action == "keep" and chance>=4 and chance<=5:
            print "Though your car wears-off a bit, it's still performing well enough.\n"
            return "goodcar"
        
        if action == "tinker" and chance<=3:
            print "The crew makes the necessary adjustments, but they are so slow...\n"
            return "slowstop"
        
        if action == "tinker" and chance>3:
            print "The crew does a nice job and saves you plenty of time on the track.\n"
            return "fastcrew"

        else:
            print "Either enter 'keep' or 'tinker'. Or CTRL-D, asshole.\n"
            return "goodcar"

class Fastcrew(Event):

    def enter(self):
        print "You've decided to make a pit-stop and your over-the-wall pit-crew does a"
        print "fantastic job. Do you 'congratulate' them or 'hold' your applause?"

        action = raw_input(">>> ")
        chance = randint(1,2)

        if action == "congratulate":
            print "Your positivity is refreshing. Josh Browne and the crew continue"
            print "their high morale for the next segment or two.\n"
            return "goodcar"

        if action == "hold" and chance==1:
            print "Your professionalism is admired and rewarded by a great setup to"
            print "go along with your excellent track position.\n"
            return "goodcar"

        if action == "hold" and chance==2:
            print "Bad karma, dude. Your car is junk.\n"
            return "wreck"

        else:
            print "Hey, guy. Pick 'congratulate' or 'hold'. OK?\n"
            return "fastcrew"


class Map(object):

    events = {
        'neutral' : Neutral(),
        'fastcrew' : Fastcrew(),
        'badcar' : Badcar(),
        'slowstop' : Slowstop(),
        'win' : Win(),
        'lose' : Lose(),
        'wreck' : Wreck(),
        'goodcar' : Goodcar()
    }

    def __init__(self, start_event):
        self.start_event = start_event

    def next_event(self, event_name):
        val = Map.events.get(event_name)
        return val

    def opening_event(self):
        return self.next_event(self.start_event)


a_map = Map('neutral')
a_game = Engine(a_map)
a_game.play()