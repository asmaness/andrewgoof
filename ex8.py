# variable that denotes four times over a printable represetnation of an object
# (whereas %s denotes something that is a bit less literal)
formatter = "%r %r %r %r"

# This line should just return 1 2 3 4 
print formatter % (1, 2, 3, 4)

# this format should be 'one' 'two' ... with %r
print formatter % ("one" , "two" , "three" , "four")

# again, repr (%r) is literal, so only True False... should be printed
print formatter % (True, False, True, False)

# this should print out '%r %r %r %r' ...
print formatter % (formatter, formatter, formatter, formatter)

# the commas after each line indicate that each phrase will be printed on the same line
# despite being on different lines in the code...  each phrase ought to be offset with
# quotations because of %r...   
print formatter % (
    "I had this thing.",
    "That you could type up right.",
    "But it didn't sing.", # shows as double quotes in output... why? maybe only two phrase at a time...? idk
    "So I said goodnight."
)