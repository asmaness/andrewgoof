# this is a python program that will calculate
# the value of an investment at any year into
# the future...

def main():

  name = raw_input("Hi, there. What is your name? ")
  
  print "Hello, " + name + ". Let's find the future value of your investment. Get excited!"
  print " "
  
  years = eval(raw_input("Tell me, " + name + ". How many years into the future are you interested in? "))
  principal = eval(raw_input("How much money are you investing? "))
  apr = eval(raw_input("And what is the annual percent rate? "))

  print " "
  print "Calculating..."
  print " "

  for i in range(years):

    principal = principal * (1 + apr/100)
    if (i==years-1) : print "The future value of your investment is ${0:0.2f}".format(principal)

main()
