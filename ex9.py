# going to be some new things in this code... string functions still

days = "Mon Tue Wed Thu Fri Sat Sun"

# we learn here that "\n" creates a new line of output within the string of
# coded data. so this is one way to keep of train of thought in code without
# having to create several lines. this is an efficient method compared to 
# simply adding months1,months2,...,months12. 
# UPDATE: Can only show the first 8 lines + the final desired line. hmm...
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug\nSep\rOct\rNov\rDec"

# just printing off the previous variables created above:
print "Here are the days: " , days
print "Here are the months: " , months

# three quotes to open and close a print. interesting. this allows the output
# to look the same as how we are typing the string in the code
print """
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6..."
"""