# creates a mapping of state to abbreviation
# the format is full state name : abbreviation
states = {
    "Oregon" :  "OR",
    "Florida" : "FL",
    "California" : "CA",
    "New York" : "NY",
    "Michigan" : "MI"
}

# create a basic set of state and some cities in them
# this variables maps various abbreviations to big cities
# that the states of those abbrev's contain...
cities = {
    "CA" : "San Francisco",
    "MI" : "Detroit",
    "FL" : "Jacksonville"
}

# add some more cities (different way to map....)
# these will now be stores in the cities variable
cities["NY"] = "New York"
cities["OR"] = "Portland"

# print out some cities
print "-" * 10 # starting a list... pretty lines....
print "NY State has: " , cities["NY"]
print "OR State has: " , cities["OR"]

# print some states...
print "-" * 10
print "Michigan's abbreviation is: " , states["Michigan"]
print "Florida's abbreviation is: " , states["Florida"]

# do it by using the state then cities dictionary
# practicing double-indexing -- take the first line for
# example... work in to out -- in the states variable,
# michigan maps to MI. so in the cities variable; MI
# maps to detroit -- the final answer...
print "-" * 10
print "Michigan has: " , cities[states["Michigan"]]
print "Florida has: " , cities[states["Florida"]]

# print every state abbreviation -- interesting technique.
# for each (state,abbrev) mapping in the states variable,
# print out that sentence. so it goes thru all 5 mappings
# in 'states' and print them out below...
print "-" * 10
for state , abbrev in states.items():
    print "%s is abbreviated %s" % (state , abbrev)

# print every city in state. similar to previous example...
# taking each (abbrev,city) mapping in the cities variable
# and prints each corresponding token out below. there are
# five sets, so there ought to be five lines.
print "-" * 10
for abbrev , city in cities.items():
    print "%s has the city %s" % (abbrev , city)

# now do both at the same time. a double indexing, if you will.
# taking the (state,abbrev) combo in the states variable in order
# to ultimately find the city. the first elements are easy -- state
# and abbrev... but to find cities, have to use indexing: use the
# corresponding abbreviation to find the city in the cities variable.
print "-" * 10
for state , abbrev in states.items():
    print "%s state is abbreviated %s and has city %s" % (
        state , abbrev , cities[abbrev])


print "-" * 10
# safely get an abbreviation by state that may not be there.
# calling a new variable 'state' and it is asking 'states' for the
# Texas abbrev. since no Texas, we create an if-not stmt that print
# we don't have it.
state = states.get("Texas")
if not state:
    print "Sorry. No Tejas."

# get a city with a default value; 
city = cities.get("TX" , "Does Not Exist")
print "The city for the state 'TX' is: %s" % city